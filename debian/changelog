istgt (0.4~20111008-4) unstable; urgency=medium

  [ Jessica Clarke ]
  * Team upload.

  [ Sudip Mukherjee ]
  * Fix ftbfs with GCC-10. (Closes: #957380)

  [ Jessica Clarke ]
  * Bump debhelper compat to 13; no changes needed
  * Drop build dependency on autotools-dev now implied by compat level
  * Change priority from deprecated extra to optional
  * Bump standards version to 4.5.1; no changes needed
  * Use debian/install rather than custom override_dh_install
  * Update Vcs-* to point to Salsa repository
  * Set Rules-Requires-Root to no
  * Fix copyright paragraph order, whitespace and URL
  * Run wrap-and-sort -ast

 -- Jessica Clarke <jrtc27@debian.org>  Sat, 28 Nov 2020 17:01:11 +0000

istgt (0.4~20111008-3) unstable; urgency=low

  * Fix "cannot determine device size from symlink" Apply patch to use stat()
    instead of lstat(). Thanks Michal Suchanek  (Closes: #650212). I apologize
    for the delay, but I was postponing this decision for a long time, given
    it was rejected by upstream for security concerns. I do not share these.
  * Fix "Add support to reload the istgt.conf file." by merging the excellent
    patch Andrew Ruthven <andrew@etc.gen.nz>. Much appreciated (Closes: #679055)
  * Improve init script:
    + Better support fancy outputs
    + Improve failure toleance of the reload handler
    + Log istgt start output to syslog

 -- Arno Töll <arno@debian.org>  Wed, 27 Jun 2012 00:23:23 +0200

istgt (0.4~20111008-2) unstable; urgency=low

  * Fix "istgt: ftbs with ld --as-needed" - apply patch which shifts libraries
    in the compiler line after object files. Thanks Julian Taylor
    (Closes: #646272)
  * Fix "ftbfs on squeeze" Add missing build-dependency to dpkg-dev 1.16.1 which
    is required to build the package because we're using dpkg-buildflags to
    generate configure flags. That's guaranteed for Testing, but might be
    helpful to know for backporters. (Closes: #650217)
  * Push standards version to 3.9.3.1 - no further changes required
  * Drop the DMUA field - I don't need it anymore, update my maintainer address

 -- Arno Töll <arno@debian.org>  Sat, 31 Mar 2012 17:13:43 +0200

istgt (0.4~20111008-1) unstable; urgency=low

  * New upstream release
  * Add patch `add-istgtcontrol-manpage' which adds a manpage for the
    istgtcontrol command.
  * Check whether istgt is actually running after starting up the daemon in its
    init script. That's required because istgt may fail for the socket setup
    after forking into the background.

 -- Arno Töll <debian@toell.net>  Thu, 27 Oct 2011 01:40:23 +0200

istgt (0.4~20110928-1) unstable; urgency=low

  * New upstream release
  * Adapt to dpkg 1.16.1 API changes regarding build flags. This enables
    hardening build flags. This means, istgt is now being built with
    -fstack-protector and other security related build flags.

 -- Arno Töll <debian@toell.net>  Fri, 07 Oct 2011 10:54:47 -0400

istgt (0.4~20110907-1) unstable; urgency=low

  * New upstream release
  * Make debian/copyright more readable
  * Provide a non-functional watch file to follow glibc-bsd conventions to
    denote the problem with it in a comment
  * Fix a not detected build failure due to a problem in upstream's Makefile
    for the GNU/Hurd architecture (`make-build-failures-fatal.patch'). This
    patch has been forwarded and merged upstream
  * Remove `handle-destdir.patch' - it has been merged upstream
  * Refresh `fix-installdir.patch'

 -- Arno Töll <debian@toell.net>  Wed, 14 Sep 2011 22:40:25 +0200

istgt (0.4~20110717-1) unstable; urgency=low

  [ Arno Töll ]
  * Make upstream Makefile honor $(DESTDIR) (`handle-destdir.patch')
  * Install binaries to /sbin instead of /bin (`fix-installdir.patch')
  * Initial release. (Closes: #636139: ITP: istgt -- iSCSI userspace
    target daemon for Unix-like operating systems)

  [ Robert Millan ]
  * Require libcam-dev on kfreebsd-any.
  * Set Dm-Upload-Allowed: yes.
  * Add myself to Uploaders.

 -- Robert Millan <rmh@debian.org>  Mon, 01 Aug 2011 00:12:35 +0200
